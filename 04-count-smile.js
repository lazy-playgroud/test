//Valid smiley face examples: :) :D ;-D :~)
function countSmileys(arr) {
  return arr.reduce((count, face) => count + /^[:;][-~]?[)D]$/.test(face), 0);
}

console.log(countSmileys([":)", ";(", ";}", ":-D"])); // should return 2;
console.log(countSmileys([";D", ":-(", ":-)", ";~)"])); // should return 3;
console.log(countSmileys([";]", ":[", ";*", ":$", ";-D"])); // should return 1;
