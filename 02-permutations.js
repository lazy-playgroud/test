// This function generates all permutations of a string
function generatePermutations(str) {
  if (str.length <= 1) {
    return [str];
  }

  const allPerms = [];
  const uniqueChars = new Set(str.split(""));

  uniqueChars.forEach((char) => {
    const remainingStr = str.replace(char, "");
    const permutationsOfRemaining = generatePermutations(remainingStr);

    permutationsOfRemaining.forEach((perm) => {
      allPerms.push(char + perm);
    });
  });

  return allPerms;
}

// This function filters out permutations that contain a character repeated 3 times in a row
function filterPermutations(permutations) {
  return permutations.filter((perm) => !perm.match(/(.)\1\1/));
}

// This function generates all permutations of a string and then filters out the invalid ones
function getValidPermutations(str) {
  const allPerms = generatePermutations(str);
  const uniquePerms = [...new Set(allPerms)];
  return filterPermutations(uniquePerms);
}

//test case1
let input1 = "a";
let permutations1 = getValidPermutations(input1);

//test case2
let input2 = "ab";
let permutations2 = getValidPermutations(input2);

//test case3
let input3 = "abc";
let permutations3 = getValidPermutations(input3);

//test case4
let input4 = "aabb";
let permutations4 = getValidPermutations(input4);

console.log(permutations1);
console.log(permutations2);
console.log(permutations3);
console.log(permutations4);
