
```sh
.
src/
  ├── app/ # controller ,module and dto
  │   ├── v1/ # app version control
  │   │   ├── controlers/ # e.g. orders is controller of orders
  │   │   │   ├── constants/ # (optional) constant value and Enum of controller
  │   │   │   ├── dto/ # DTO (Data Transfer Object) of controller
  │   │   │   ├── interfaces/ # (optional) typescript interface of controller
  │   │   │   ├── types/ # (optional) typescript type of controller
  │   ├── auth.module.ts
  ├── common/ # core custom functions ,global class constants and interface (dto ,decorators ,interceptors ,constants ,interfaces ,types)
  │   ├── constants/ # global constants
  │   ├── decorators/
  │   ├── dto/ # global DTO (Data Transfer Object)
  │   ├── functionals/ # utility functions
  │   ├── interceptors/ # nest typescript interceptors
  │   ├── interfaces/ # global DTO (Data Transfer Object)
  │   ├── validators/ # custom validate use for dto
  │   ├── index.ts # export all file in common 
  ├── config/ # configuration database (typeorm) and etc.
  │   ├── database/
  │   │   ├── type-orm.ts
  ├── entities/ # model ,criteria and transformers 
  │   ├── criterias/
  │   ├── transformers/
  │   ├── builder-criteria.ts
  │   ├── repository.interface.ts
  ├── exceptions/ # exception filter and custom exception etc.
  ├── guard/ # nest guards is same middleware (Authentication and Authorization)
  ├── providers/ # optional for used micro service
  ├── routes/ # nest route module
  │   ├── v1/ # routing version control
  ├── services/ # logic of controller
  │   ├── v1/ # service version control
  ├── shared/ # module for sharing (logging ,cache ,local-logging)
  │   ├── logging/ # logging with @wepos-logging
  │   ├── redis-cache/ # redis caching
  ├── app.module.ts
  └── main.ts
test/ # e2e testing
```