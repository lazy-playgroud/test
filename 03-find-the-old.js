function findOddOccurrence(arr) {
  const counts = new Map();

  arr.forEach(function (num) {
    counts.set(num, counts.has(num) ? counts.get(num) + 1 : 1);
  });

  for (let [num, count] of counts) {
    if (count % 2 !== 0) {
      return num;
    }
  }
  return null;
}

// Test case
console.log(findOddOccurrence([7])); // should return 7
console.log(findOddOccurrence([0])); // should return 0
console.log(findOddOccurrence([1, 1, 2])); // should return 2
console.log(findOddOccurrence([0, 1, 0, 1, 0])); // should return 0
console.log(findOddOccurrence([1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1])); // should return 4
